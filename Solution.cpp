#include <iostream>

//вариант 1

using namespace std;

int getRandNums(int* randNums);
int checkHits(double x, double y, int* arrNums, int* randNums, int* hits, int* missed);
bool checkRep(int* arrNums, int* randNums);

int main() {
    int randomNums = 40;
    cout << getRandNums(&randomNums) << "%" << endl;
    return 0;
}

bool checkRep(int* arrNums, int* randNums) {
    for (int i = 0; i < *randNums; i++) {
        for (int j = i + 1; j < *randNums; j++) {
            if (arrNums[i] == arrNums[j]) {
                return true;
            }
        }
    }
    return false;
}

int checkHits(double x, double y, int* arrNums, int* randNums, int* hits, int* missed) {
    if (-1.5 < -x - y && -1.5 < x - y && 1.5 > -x - y && 1.5 > x - y &&
        y < 1 && y > -1 && x < 1 && x > -1) {
        (*hits)++;
    }
    else {
        (*missed)++;
    }
    return 0;
}

int getRandNums(int* randNums) {
    int* arrOfHits = new int[*randNums];
    int hits = 0;
    int missed = 0;
    for (int i = 0; i < *randNums; i++) {
        double x = (double)rand() / (double)RAND_MAX * (.5 + .5) + .5;
        double y = (double)rand() / (double)RAND_MAX * (.5 - (-.5));
        arrOfHits[i] = x + y;
        if (checkRep(arrOfHits, randNums) == false) {
            break;
        }
        checkHits(x, y, arrOfHits, randNums, &hits, &missed);
    }

    return (hits * 100) / (*randNums);
}
